<?php

/* standar.html */
class __TwigTemplate_cf037bc202f4a71251fd9569d17a5ef17d54518ea32431f761256cf59abbbe6e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["campos"]) ? $context["campos"] : null));
        foreach ($context['_seq'] as $context["key"] => $context["valor"]) {
            // line 2
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo ": ";
            echo twig_escape_filter($this->env, $context["valor"], "html", null, true);
            echo " <br/>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['valor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "standar.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 2,  19 => 1,);
    }
}
/* {% for key, valor in campos %}*/
/* {{ key }}: {{ valor }} <br/>*/
/* {% endfor %}*/
/* */
