<?php

require_once __DIR__ . '/../include/leads-notifier-functions.php';

$action = $_REQUEST['action'];

if($action==="subscribe"){
 $page_id = $_REQUEST['page_id'];
 $page_name = $_REQUEST['page_name'];
 $email = $_REQUEST['email'];
 $company = $_REQUEST['company'];

 try{
 
 $mysql=get_database_instance();
 $pre=$mysql->prepare("replace into lead_notifier_client (page_id, name, email, empresa) values (?,?,?,?)");
 $pre->bind_param("isss",$page_id,$page_name,$email,$company);

  if(!$result = $pre->execute()){
   throw new Exception("El registro de la pagina $page_name a la base de datos ha fallado!");
  }


}
catch(Exception $e){
  error_log($e->getMessage());
  $mail=get_email_sender_instance();
  $mail->addAddress("info@artesyweb.com");
  $mail->addBCC("pabloandi@gmail.com");
  $mail->Subject("Error en el registro de subscription de la pagina $page_name");
  $mail->Body = $e->getMessage();
  $mail->send();
  exit;
}
}
else if($action==="unsubscribe"){
 $page_id = $_REQUEST['page_id'];

 try{

 $mysql=get_database_instance();
 $pre=$mysql->prepare("delete from lead_notifier_client where page_id = ?)");
 $pre->bind_param("i",$page_id);

  if(!$result = $pre->execute()){
   throw new Exception("La eliminacion del registro de la pagina $page_name a la base de datos ha fallado!");
  }


}
catch(Exception $e){
  error_log($e->getMessage());
  $mail=get_email_sender_instance();
  $mail->addAddress("info@artesyweb.com");
  $mail->addBCC("pabloandi@gmail.com");
  $mail->Subject("Error en el borrado de la subscripcion de la pagina $page_name");
  $mail->Body = $e->getMessage();
  $mail->send();
  exit;
}

}
else if($action==="data"){
try{
 
 $mysql=get_database_instance();
  if(!$result = $mysql->query("select page_id, email, empresa from lead_notifier_client")){
   throw new Exception("La consulta de la pagina $page_id a la base de datos ha fallado!");
  }
  if($result->num_rows === 0){
    throw new Exception("No se ha encontrado la pagina $page_id en la base de datos");
  }

  $pages=array();

  while($page = $result->fetch_assoc()){  
	$pages[$page['page_id']]=array($page['email'],$page['empresa']);
  }
}
catch(Exception $e){
  error_log($e->getMessage());
  $mail=get_email_sender_instance();
  $mail->addAddress("info@artesyweb.com");
  $mail->addBCC("pabloandi@gmail.com");
  $mail->Subject("Error en el envio de notificación en $page_id");
  $mail->Body = $e->getMessage();
  $mail->send();
  exit;
}

//http_response_code(200);
header('Content-Type: application/json; charset=utf-8');
echo json_encode($pages);

}
