 <head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head> 
<h2 class="text-center">Subscripción de clientes a Facebook Leads</h2>

<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '806832029448825', //cambiar la id de app
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

 function subscribeApp(page_id, page_access_token){
  console.log('Subscribing page to app ' + page_id );
  FB.api(
   '/' + page_id + '/subscribed_apps',
   'post',
   {access_token: page_access_token},
   function(response){
	if (!response || response.error) {
		alert("Error en la subscripcion de la aplicacion ");
	}
	else{
		alert("Se ha subscrito la aplicacion correctamente");
		console.log('successfully subscribe page', response);
		jQuery("#row-"+page_id).addClass("bg-info");
	}
  });
 }

 function unsubscribeApp(page_id, page_access_token){
  console.log('Subscribing page to app ' + page_id );
  FB.api(
   '/' + page_id + '/subscribed_apps',
   'delete',
   {access_token: page_access_token},
   function(response){
	if (!response || response.error) {
                alert("Error en el borrado de subscripcion de la aplicacion ");
        }
        else{
                alert("Se ha borrado la subscripcion de la aplicacion correctamente");
                console.log('successfully subscribe page', response);
		jQuery("#row-"+page_id).removeClass("bg-info");
        }

  });
 }


  // Only works after `FB.init` is called
  function myFacebookLogin() {
    FB.login(function(response){
	console.log('Successfully logged in', response);
        FB.api('/me/accounts', function(response){
	 console.log('Successfully retrieved pages', response);
	jQuery("#loading").show();
         loadSubscribePages(response);
	jQuery("#loading").hide();

	//retrievePagesData();
	 
	});
    }, {scope: 'manage_pages'});
  }

  function loadSubscribePages(response){

	var pages = response.data;
         var ul = document.getElementById('list');
          var pagerup = document.createElement('div');
	 var linkbefore = document.createElement('a');
         var linkafter = document.createElement('a');
         var br = document.createElement('br');
	 linkbefore.href='#';
           linkafter.href='#';
	   linkbefore.id="linkbefore";
	   linkafter.id="linkafter";
           linkbefore.className='btn btn-default';
           linkafter.className='btn btn-default';
           linkbefore.innerHTML="<span class='glyphicon glyphicon-menu-left'>Anterior</span>";
           linkafter.innerHTML="<span class='glyphicon glyphicon-menu-right'>Siguiente</span>";
           linkbefore.onclick=generatePagerLink.bind(this,response.paging.previous);
	   linkafter.onclick=generatePagerLink.bind(this,response.paging.next);
	linkbefore.setAttribute("data-loading-text","<i class='icon-spinner icon-spin icon-large'></i>");   
	linkafter.setAttribute("data-loading-text","Cargando...");
	
	  pagerup.appendChild(linkbefore);
          pagerup.appendChild(linkafter);
          pagerup.className="row text-center";
          ul.appendChild(pagerup);
          ul.appendChild(br);

         for(var i=0, len = pages.length; i < len; i++){
          var page = pages[i];
          var row = document.createElement('div');
          var cname = document.createElement('div');
          var cemail = document.createElement('div');
          var ccompany = document.createElement('div');
          var csubscribe = document.createElement('div');
          var cunsubscribe = document.createElement('div');
          var linksubscribe = document.createElement('a');
          var linkunsubscribe = document.createElement('a');
          linksubscribe.href='#';
          linksubscribe.className='btn btn-default';
          linksubscribe.onclick=subscribePageToLeadApp.bind(this,page);
          linksubscribe.innerHTML="<span class='glyphicon glyphicon-ok'>Subscribir</span>";
          linkunsubscribe.href='#';
          linkunsubscribe.className='btn btn-default';
          linkunsubscribe.onclick=subscribePageToLeadApp.bind(this,page);
          linkunsubscribe.innerHTML="<span class='glyphicon glyphicon-ok'>Quitar</span>";


          row.className="row";
          row.id="row-"+page.id;
          cname.className="col-md-3";
          cemail.className="col-md-3";
          ccompany.className="col-md-2";
          csubscribe.className="col-md-2";
          cunsubscribe.className="col-md-2";
          cname.innerHTML=page.name;
          cemail.innerHTML="<input type='text' class='form-control' placeholder='email' id='email-"+page.id+"'/>";
          ccompany.innerHTML="<input type='text' class='form-control' placeholder='copia oculta' id='company-"+page.id+"'/>";
          csubscribe.appendChild(linksubscribe);
          cunsubscribe.appendChild(linkunsubscribe);
          row.appendChild(cname);
          row.appendChild(cemail);
          row.appendChild(ccompany);
          row.appendChild(csubscribe);
          row.appendChild(cunsubscribe);
          
          ul.appendChild(row);
	}

	retrievePagesData();

  }

  function generatePagerLink(u){
	if(u){
                        jQuery.ajax({
                         method: 'get',
                         url: u,
			 dataType: 'json',
			 beforeSend: function(){
				jQuery("#loading").show();
			 }
                        }).done(function(r){
				jQuery("#list").html("");
				loadSubscribePages(r);
				jQuery("#loading").hide();
                        });
                 }

  }



  function subscribePageToLeadApp(page){
   // var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var re = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
    
    var emails = jQuery("#email-"+page.id).val();
    var company = jQuery("#company-"+page.id).val();
    var email_validado=true;
   
    if(emails!=''){ 
    jQuery.each(emails.split(","),function(index,email){
      if(!email.match(re)){
	email_validado= false;
      }
    });
    }
    else
	email_validado=false;
    
    if(email_validado){
     jQuery.ajax({
	method: 'post',
	url: 'subscribe.php',
 	data: { page_id: page.id, page_name: page.name, email: emails, company: company, action: 'subscribe' }
     })
     .done(function(){
	subscribeApp(page.id, page.access_token);
     });
    }
    else
	alert("El campo correo de '"+page.name+"' tiene un problema");
  }

  function unsuscribePageToLeadApp(page){
     jQuery.ajax({
        method: 'post',
        url: 'subscribe.php',
        data: { page_id: page.id, action: 'unsubscribe' }
     })
     .done(function(){
        unsubscribeApp(page.id, page.access_token);
	
     });

  }

 function retrievePagesData(){
    jQuery.ajax({
        method: 'post',
        url: 'subscribe.php',
	dataType: 'json',
        data: { action: 'data' }
    })
    .done(function( data ){
	console.log(data);
      	jQuery.each(data, function(page_id,pagedata){
		jQuery("#email-"+page_id).val(pagedata[0]);
		jQuery("#company-"+page_id).val(pagedata[1]);
		jQuery("#row-"+page_id).addClass("bg-info");
	});
    });
 
}
</script>
<div class="container text-center"><button onclick="myFacebookLogin()">Entrar con Facebook</button></div>
<div class="container text-center"><img src="images/loading.gif" id="loading" style="display:none" /></div>
<br/>
<div class="container" id='list'></div>
