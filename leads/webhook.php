<?php

require_once __DIR__ . '/../include/leads-notifier-functions.php';
require_once __DIR__ . '/../include/facebook-functions.php';


//error_log(print_r($_REQUEST, true));
// Verifying the verify_token ensures nobody else can
// subscribe random things to your application.
if ($_REQUEST['hub_verify_token'] === 'fc344697436fb573edbcbd25a9c4e7d5') {
  echo $_REQUEST['hub_challenge'];
}
else{
// exit;
}

// configuration
$app_id= "806832029448825";
$app_secret= "c1915c3b76b2de52e318499a805a5b83";
$access_token= "CAALdzziCOnkBADyG9j8htg27mPIZAO3ZAamMB47ykjzgNtEBBLkqCKLtfEOo4eb0ptqZAHuwZBVB0w6BuVwJ69r7jgRPeNaRnWDqeg7ujCrx4zZCQQLPw034SZCEbhQDxXqCOZBC2gPkm8ffHabeWPJNzQjdm7vVZB4pMTlwTucHZCzjTj1E5snguATWHyRB71Fkmcjzof8dfNgZDZD";

$input = json_decode(file_get_contents('php://input'), true);
//error_log(print_r($input, true));

foreach($input['entry'] as $entry){
 foreach($entry['changes'] as $change){

  $lead_id = $change['value']['leadgen_id'];
  $page_id = $change['value']['page_id'];


if(is_null($lead_id) or is_null($page_id)){
 error_log("no existe el cliente potencial");
}
else{

try{
  $data=fb_get_data_lead(
    $lead_id,
    //App ID
    $app_id,
    //App Secret
    $app_secret,
    //Access_token
    $access_token
  );

  $mysql = get_database_instance();
  if(!$result = $mysql->query("select * from lead_notifier_client where page_id = $page_id limit 1")){
   throw new Exception("La consulta de la pagina $page_id a la base de datos ha fallado!");
  }
  if($result->num_rows === 0){
    throw new Exception("No se ha encontrado la pagina $page_id en la base de datos");
  }

  $page = $result->fetch_assoc();
  
}
catch(Exception $e){
  $mail=get_email_sender_instance();
  $mail->addAddress("info@artesyweb.com");
  $mail->addBCC("pabloandi@gmail.com");
  $mail->Subject("Error en el envio de notificación en $name_page");
  $mail->Body = $e->getMessage();
  $mail->send();
  exit;
}

$lead = get_lead_data_array($data);
//error_log(print_r($lead, true));

// correo del cliente a enviar las notificaciones
send_email_notification($page['email'], "Formulario lead ".$page['name'], $lead,$page['company']);

}

}}
