<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../include/leads-notifier-functions.php';

use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use FacebookAds\Api;
use FacebookAds\Object\Lead;


function fb_renew_access_token($app_id, $app_secret, $old_token){
     $token_url = "https://graph.facebook.com/oauth/access_token?client_id=$app_id&client_secret=$app_secret&grant_type=fb_exchange_token&fb_exchange_token=$old_token";

     $c = curl_init();
     curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
     curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
     curl_setopt($c, CURLOPT_URL, $token_url);
     $contents = curl_exec($c);
     $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
     curl_close($c);

     $paramsfb = null;
     parse_str($contents, $paramsfb);

     return $paramsfb['access_token'];
 }

function fb_get_data_lead($lead_id, $app_id, $app_secret, $access_token){
 
 $access_token = fb_renew_access_token(
   $app_id, 
   $app_secret, 
   $access_token
 ); 

  // Initialize a new Session and instantiate an Api object
  Api::init(
    $app_id, // App ID
    $app_secret, //App secret
    $access_token //Access Token
  );

  $form = new Lead($lead_id);
  $data=$form->read();
  return $data;

}
